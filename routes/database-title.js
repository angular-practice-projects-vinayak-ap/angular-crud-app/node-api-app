const express = require("express");
const router = express.Router();
const UserService = require("../services/user.service");

/* Fetching Database Title From Database */
router.get("/", async (req, res, next) => {
  try {
    const dataTitle = await UserService.getDataTitle();
    return res.status(200).json(dataTitle.data);
  } catch (err) {
    return next(err); // unexpected error
  }
});

module.exports = router;
