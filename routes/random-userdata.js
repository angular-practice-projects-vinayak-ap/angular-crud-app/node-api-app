const express = require("express");
const router = express.Router();
const UserService = require("../services/user.service");

/* Fetching Random User Data */
router.get("/", async (req, res, next) => {
  try {
    const randomUser = await UserService.getRandomUserData();
    return res.status(200).json(randomUser.data);
  } catch (err) {
    return next(err); // unexpected error
  }
});

module.exports = router;
