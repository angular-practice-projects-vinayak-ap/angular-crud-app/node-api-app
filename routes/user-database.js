const express = require("express");
const router = express.Router();
const UserService = require("../services/user.service")

//Handle incoming GET request to /users
router.get("/", async (req, res, next) => {
  try {
    const user = await UserService.getUser();
    return res.status(200).json(user.data);
  } 
  catch (err) {
    return next(err); // unexpected error
  }
});

router.post("/", async (req, res, next) => {
  try {
    const user = await UserService.createUser(req.body);
    return res.status(201).json(user.data);
  } 
  catch (err) {
    if (err.name === "ValidationError") {
      return res.status(400).json(err.errorObj);
    } 
    /* unexpected error */
    else return res.status(400).json({
      name: "Unexpected Error",
      error: err,
    });
  }
});

module.exports = router;
