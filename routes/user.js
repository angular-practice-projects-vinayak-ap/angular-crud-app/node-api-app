const express = require("express");
const router = express.Router();
const UserService = require("../services/user.service");

router.put("/:uid", async (req, res, next) => {
  const uid = req.params.uid;
  const userData = req.body;
  try {
    const user = await UserService.updateUser(uid, userData);
    return res.status(201).json(user.data);
  }
  catch (err) {
    if (err.name === "ValidationError") {
      return res.status(400).json(err.errorObj);
    } 
    /* unexpected error */
    else return res.status(400).json({
      name: "Unexpected Error",
      error: err,
    });
  }
});

router.delete("/:uid", async (req, res, next) => {
  const uid = req.params.uid;
  try {
    const user = await UserService.deleteUser(uid);
    return res.json({ success: true });
  }
  catch (err) {
    return next(err); // unexpected error
  }
});

module.exports = router;
