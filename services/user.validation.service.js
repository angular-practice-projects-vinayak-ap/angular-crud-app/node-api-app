const Validator = require("fastest-validator");

let userValidator = new Validator();
let namePattern = /^(?=.{1,50}$)[a-z]+(?:['_.\s][a-z]+)*$/i;
const userSchema = {
  firstName: { type: "string", min: 1, max: 50, pattern: namePattern },
  lastName: { type: "string", min: 1, max: 50, pattern: namePattern },
  email: { type: "email", max: 75 },
};

class ValidationService {
  
  static userValidation(userData) {
    var userVal = userValidator.validate(userData, userSchema);
    /* -- If Validation Fails -- */
    if (!(userVal === true)) {
      throw {
        name: "ValidationError",
        errorObj: userVal,
      };
    } else {
      return true;
    }
  }
}

module.exports = ValidationService;
