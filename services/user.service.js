const ValidationService = require("./user.validation.service");
const axios = require("axios");

const url = "https://angular-crud-web-app-default-rtdb.firebaseio.com/userdatabase.json";
const dataTitleUrl = 'https://angular-crud-web-app-default-rtdb.firebaseio.com/databaseTitle.json';
const randomUserUrl = 'https://randomuser.me/api';


class UserService {

  /* -- Adding User To The Database -- */ 
  static async createUser(userData) {
    const isValid = ValidationService.userValidation(userData)
    if (isValid) {
      return await axios.post(url, userData);
    } 
  }

  /* -- Fetching User Data From Database -- */
  static async getUser() {
    return await axios.get(url);
  }

  /* -- Updating User Data In The Database -- */
  static async updateUser(uid, userData) {
    const userIdUrl = `https://angular-crud-web-app-default-rtdb.firebaseio.com/userdatabase/${uid}.json`;
    const isValid = ValidationService.userValidation(userData)
    if (isValid) {
      return await axios.put(userIdUrl, userData)
    }
  }

  /* -- Deleting User From The Database -- */
  static async deleteUser(uid) {
    const userIdUrl = `https://angular-crud-web-app-default-rtdb.firebaseio.com/userdatabase/${uid}.json`;
    return await axios.delete(userIdUrl);
  }

  /* -- Fetching Databse Title From Database -- */
  static async getDataTitle() {
    return await axios.get(dataTitleUrl);
  }

  /* -- Fetching Random User Data From Web -- */
  static async getRandomUserData() {
    return await axios.get(randomUserUrl);
  }
}

module.exports = UserService;

