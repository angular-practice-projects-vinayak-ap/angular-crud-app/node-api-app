const express = require("express");
const app = express();
const bodyParser = require("body-parser");

const userDatabaseRoutes = require("./routes/user-database");
const userRoutes = require("./routes/user");
const dataTitleRoutes = require("./routes/database-title");
const randomUserRoutes = require("./routes/random-userdata");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
})

//Handling Routes
app.use("/api/users", userDatabaseRoutes);
app.use("/api/users", userRoutes);
app.use("/api/dataTitle", dataTitleRoutes);
app.use("/api/randomUser", randomUserRoutes);

//Handling Errors
app.use((req, res, next) => {
  const error = new Error("Page Not Found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

module.exports = app;
